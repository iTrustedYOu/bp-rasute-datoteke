#include <stddef.h>//OVO NIJE NORMALNO
#include <assert.h>
#include "Bucket.h"
#include "terminNastave.h"

#define FAKTOR_BAKETIRANJA 5

struct bucket{
   int emptySpaceCounter;
   TerminNastave nizTermina;
};
typedef struct bucket bucket;

TerminNastave get_termin_address(int i, Bucket  b);


//PITAJ ZASTO OVDE DOLE POMNOZI SVAKI DECIMALNI BROJ SA 4. KO TO RADI UOPSTE??

//NE RAZUMEM ZASTO MAJKU MU mi daje 8 pozicija zaaaa oznaku. nesto mi nije tu jasno
//
//mislim da bi alokaciju trebao da radi korisnik, a init da ovo bude samo.
void init_bucket(Bucket b){
   //ovako smo oznacili da je bucket==bucket termin_nastave[FAKTOR_BAKETIRANJA];
   int i;
   b->emptySpaceCounter=FAKTOR_BAKETIRANJA;
   for(i=0;i<FAKTOR_BAKETIRANJA;++i){
      TerminNastave tn=get_termin_address(i,b);
      init_termin(tn);//i tako smo ovaj deo memorije popunili
      //ne moze ovako da se svaki termin inicira. nista nam ne garantuje
      //da ce biti jedan pored drugog;
   }

}
//dajemo redni broj u baketu, i baket
//ovo je kao neki [] operator
//znaci adresa terminNastave pokazivacu i onda offset u njemu
//POKAZIVACI KAKO SE SABIRAJU??
TerminNastave get_termin_address(int pozUBaketu, Bucket b){
   assert(pozUBaketu<FAKTOR_BAKETIRANJA);
   char* ptr=(char *)b +offsetof(bucket,nizTermina)+pozUBaketu*termin_size();//valjda ce sada inkrementirati po bajtovima
   return (TerminNastave) ptr;
}

void print_bucket(Bucket b){

   int i;
   for(i=0;i<FAKTOR_BAKETIRANJA;++i){
      TerminNastave tn=get_termin_address(i,b);
      if(get_key(tn)!=-1){
         printf("Adresa sloga u baketu: %d\n",i);
         ispisi_podatke(tn);
      }
   }

}
//vraca nulu ako je moguce da ce
//morati da se trazi prekoracioci u
//ostalim baketima
TerminNastave izbrisi_iz_baketa(Bucket b,const int pozUBaketu){
   int za_shift=0;
   TerminNastave ret=malloc(termin_size());
   
   TerminNastave tn_praznina=get_termin_address(pozUBaketu,b);
   termin_copy(tn_praznina,ret);
   if(pozUBaketu!=FAKTOR_BAKETIRANJA-1){
      for(za_shift=pozUBaketu+1;za_shift<=FAKTOR_BAKETIRANJA-(b->emptySpaceCounter);++za_shift){
         
         TerminNastave tn_shift=get_termin_address(za_shift,b);
         termin_copy(tn_shift,tn_praznina);
         tn_praznina=tn_shift;
         

      }
   }
   init_termin(tn_praznina);
   (++b->emptySpaceCounter);
   printf("Izbrisan element iz baketa, sad imamo %d praznih pozicija\n",b->emptySpaceCounter);
   return ret;




}

//ZNACI TODO DEALOCIRANJE MEMORIJE (SEARCH MALLOC) i KOJE FUNCKIJE DA STAVIS
//CONST ZAGLAVLJE. SIGURNO IH IMA



size_t bucket_size(){
   return termin_size()*FAKTOR_BAKETIRANJA+sizeof(int);

}



int ubaci_u_baket(Bucket b,TerminNastave tn){
   TerminNastave t;
   int i;
   for(i=0;i<FAKTOR_BAKETIRANJA;++i){
      t=get_termin_address(i,b);
      if(get_key(t)==-1) {
         termin_copy(tn,t);
         b->emptySpaceCounter--;
         printf("Uspesno ubacivanje u baket!\n");
         break;
      }
      if(i==FAKTOR_BAKETIRANJA-1){
         printf("Pun baket!\n");
         //assert(0);
         return FALSE;
      }
   }

   return TRUE;
}

int get_emptySpaceCount(Bucket b){

      return b->emptySpaceCounter;
}
void promeni_datum_vreme_baket(Bucket b, int pozicijaUBaketu){
      TerminNastave tn=get_termin_address(pozicijaUBaketu,b);
      set_datum_termin(tn);
      set_vreme_termin(tn);

}

//~ int main(int argc, char *argv[]){
      //~ Bucket b=malloc(bucket_size());
      //~ init_bucket(b);
      //~ print_bucket(b);
      //~ printf("%30s",b);
   //~
   //~ return 0;
//~ }
int trazi_odredjenog_prekoracioca( Bucket bThis, int (*get_address)(int),const int adresaBaketa){
	int pozUBaketu=-1;
	int i;
	for(i=0;i<FAKTOR_BAKETIRANJA;++i){
		TerminNastave tn=get_termin_address(i,bThis);
		if(get_address(get_key(tn))==adresaBaketa){
			//nasli smo prvog prekoracioca. ne treba nam nista drugo
			pozUBaketu=i;
			break;

		}

	}
	return pozUBaketu;

}
//~ TerminNastave ima_prekoracioca(Bucket b, int kljuc, int (*pt2AddrTrans)(int)){
   //~ for(i=0;i<FAKTOR_BAKETIRANJA;++i){
      //~ TerminNastave tn=get_termin_address(i,b);
      //~ if(pt2AddrTrans(get_key(tn))==kljuc){
         //~ izbrisi_iz_baketa(b,i);
         //~ break;
      //~ }
      //~
   //~ }
   //~ return
//~ }

TerminNastave trazi_slog(Bucket b, int kljuc, int *pozUBaketu){
   int i;
   TerminNastave ret;
   TerminNastave tn;
   for(i=0;i<FAKTOR_BAKETIRANJA;++i){
      tn=get_termin_address(i,b);
      if(get_key(tn)==kljuc){
         ret=tn;
         *pozUBaketu=i;
         break;
      }
      ret=NULL;
      }


   return ret;
}
