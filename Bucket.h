#ifndef BUCKET_H
#define BUCKET_H

#include <string.h>


#include "terminNastave.h"
typedef struct bucket* Bucket;



TerminNastave trazi_slog(Bucket bThis, int kljuc, int *pozUBaketu);
size_t bucket_size();
void print_bucket(Bucket bThis);
int get_emptySpaceCount(Bucket bThis);
void init_bucket(Bucket bThis);
int ubaci_u_baket(Bucket bThis,TerminNastave tn);
void promeni_datum_vreme_baket(Bucket bThis, int pozicijaUBaketu);
TerminNastave izbrisi_iz_baketa(Bucket b,const int pozUBaketu);
int trazi_odredjenog_prekoracioca( Bucket bThis, int (*get_address)(int),const int adresaBaketa);

#endif
