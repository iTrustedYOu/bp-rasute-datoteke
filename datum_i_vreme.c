#include <stdlib.h>
#include "datum_i_vreme.h"
#include "razne_io_provere.h"
#define TRUE 1
#define FALSE 0


int isLeapYear(int year){

   int isLeapYear;
   isLeapYear=FALSE;
   if ((year % 4 ) == 0 ) {
      isLeapYear=TRUE;
      if((year % 100)==0){
         if((year % 400)!=0){
            isLeapYear=FALSE;
         }
      }
   }

   return isLeapYear;

}



//uzima mesec i godinu, i izracunava koliko ima dana u mesecu
int get_num_of_days(meseci m, int godina){
      int numOfDays;
      switch(m){
         case FEB: numOfDays=isLeapYear(godina)?29:28;break;
         case JAN: ;
         case MAR: ;
         case MAJ: ;
         case JUL: ;
         case AVG: ;
         case OKT: ;
         case DEC: numOfDays=31;break;
         case APR: ;
         case JUN: ;
         case SEP: ;
         case NOV: numOfDays=30; break;
         
         
   }
   return numOfDays;
   
   
}




//koristicu samo ove metoda za zadavanje nekih
//vrednosti koje su ogranicene
//ovo ce mi biti interfejs
void set_vreme(Vreme v){
   printf("Unesite redni broj sata u kom pocinje nastava.\n");
   get_limited_int(&(v->sati),1,24);
  
   printf("Unesite redni broj minuta u satu u kom pocinje nastava!\n");
   get_limited_int(&(v->minuti),0,60);
   
}

void set_datum(Datum d){
   
   printf("Koje godine se odrzava nastava?\n");
   get_limited_int(&(d->godina),1900,2100);
   printf("Unesite mesec u kojem se odrzava nastava\n");
   get_limited_int((int*)&(d->mesec),JAN,DEC);
   printf("Unesite dan u mesecu na koji se odrzava nastava\n");
   get_limited_int(&(d->dan),1, get_num_of_days(d->mesec,d->godina));
   
}

void ispisi_datum(Datum d){
   printf("%d/%d/%d\n",d->dan,d->mesec,d->godina);
}
void ispisi_vreme(Vreme v){
   
   printf("%d:%d\n",v->sati,v->minuti);
}

void make_empty_datum(Datum d){
   d->dan=0;
   d->mesec=0;
   d->godina=0;
   
}

void  make_empty_vreme(Vreme v){
   v->sati=-1;
   v->minuti=-1;
}

size_t datum_size(){
   
   return sizeof(datum);
}

size_t vreme_size(){
   
   return sizeof(vreme);
}
