#ifndef DATUM_VREME_H
#define DATUM_VREME_H
#include <assert.h>

struct vreme{
    int sati;
    int minuti;

};

typedef struct vreme vreme;

struct datum{
   int dan;
   int mesec;
   int godina;

};

typedef struct datum datum;

typedef struct datum* Datum;
typedef struct vreme* Vreme;


//PITAJ DA LI POSTOJI BOLJA ALOKACIJA ZA OVAJ ADT


typedef enum {JAN=1,FEB,MAR,APR,MAJ,JUN,JUL,AVG,SEP,OKT,NOV,DEC} meseci;

int isLeapYear(int year);
void set_vreme(Vreme v);

void set_datum(Datum d);

size_t datum_size();

size_t vreme_size();

void ispisi_datum(Datum d);
void ispisi_vreme(Vreme v);

void make_empty_datum(Datum d);

void  make_empty_vreme(Vreme v);

#endif
