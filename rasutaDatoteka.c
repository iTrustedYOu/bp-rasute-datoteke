
#include <math.h>
#include <assert.h>
#include <ctype.h>
#include "terminNastave.h"
#include "Bucket.h"
#include "razne_io_provere.h"
#include "datum_i_vreme.h"


#define KORAK 1
#define BROJ_BAKETA 9
int sledeca_adresa_baketa(const int adresaBaketa);
int h(int key);
int get_address(int key);
char getChoice(void);
/***************************
 * Metode za pravljenje i biranje
 * aktivne datoteke. 
 ************************/

void napraviDatoteku();
void izaberi_aktivnu_datoteku(char *aktivnaDatoteka);
void prikazi_naziv_aktivne_datoteke(char* aktivnaDatoteka, FILE *fAktivna);

/* *************************
 * Metode za citanje, azuriranje
 * i brisanje iz aktivne rasute datoteke.
 * Pre poziva ovih metoda, vec je 
 * otvorena i izabrana aktivna datoteka
 ****************************/

Bucket trazi_slog_rasuta( int kljuc, int *adresa,FILE*fp, int *pozUBaketu);
void prikazi_sadrzaj_datoteke(FILE *fp);
void promeni_datum_vreme(int key,FILE *fp);
void upisi_slog(FILE *fp);
void izbrisi_slog_rasuta(FILE *fp);

void izvadi_prekoracioca(const int pocetnaAdresa, FILE *fp);
/****************************
 * Samo putem ovih metoda pristupamo
 * fajlovima. Tj, jedino one direktno
 * pristupaju fajlovima. One radi u jedinici
 * baketa.
 ****************************/

void fseek_rasuta(FILE *fp, int adresa, long odakle);
void fread_rasuta(Bucket b,int adresa,FILE *fp);
void fwrite_rasuta(Bucket b, int adresa, FILE *fp);





#define AKT_DAT_LEN 30




int main(int argc, char *argv[]){
   char choice;
   char aktivnaDatoteka[AKT_DAT_LEN]={42};
   FILE *fAktivna=NULL;
   
   
   
   while((choice=getChoice())!='i'){
      switch(choice){
         case 'f':napraviDatoteku();break;
         case 'a':{
            
            izaberi_aktivnu_datoteku(aktivnaDatoteka);
            fAktivna=safe_fopen(aktivnaDatoteka,"rb+");
            break;}
         case 'p':prikazi_naziv_aktivne_datoteke(aktivnaDatoteka,fAktivna);break;
         case 'u':
         {
            if(fAktivna==NULL){
               printf("Izaberite aktivnu datoteku prvo!\n");
               break;
            }
            upisi_slog(fAktivna);
            break;
         }
         case 's':
         {
            if(fAktivna==NULL){
               printf("Izaberite aktivnu datoteku prvo2!\n");
               break;
            }
            prikazi_sadrzaj_datoteke(fAktivna);
            break;
         }
         case 'd':{
            printf("Unesite evidencioni broj sloga ciji datum i vreme zelite da promenite\n");
            int key=ucitaj_cifre();
            promeni_datum_vreme(key,fAktivna);
            break;
         }
         case 'x':{
            izbrisi_slog_rasuta(fAktivna);
            break;
         }
         case 'i':printf("Izlazim. Dovidjenja,\n");break;
         default : assert(0);
      }
      fflush(fAktivna);
   }

   if(fAktivna!=NULL) safe_fclose(fAktivna);
   return EXIT_SUCCESS;
}
#define MAX_IME_LEN 30
void napraviDatoteku(){
   FILE *novaRasuta;
   Bucket b;
   char imeDatoteke[MAX_IME_LEN]={0};
   printf("Unesite naziv datoteke koju hocete da napravite\n");
   fgets_checked(imeDatoteke,MAX_IME_LEN,stdin);
   novaRasuta=safe_fopen(imeDatoteke,"wb");
   b=malloc(bucket_size());
   init_bucket(b);
   int i;
   for(i=0;i<BROJ_BAKETA;++i){
      fwrite(b,bucket_size(),1,novaRasuta);
   }
   free(b);
   b=NULL;
   safe_fclose(novaRasuta);
}
void izaberi_aktivnu_datoteku(char *aktivnaDatoteka){
   printf("Unesite naziv aktivne datoteke!\n");
   fgets_checked(aktivnaDatoteka,MAX_IME_LEN,stdin);
   printf("Izabrana '%s' datoteka za aktivnu\n",aktivnaDatoteka);
}
void prikazi_naziv_aktivne_datoteke(char* aktivnaDatoteka, FILE *fAktivna){
   if(fAktivna==NULL){
      printf("Morate prvo izabrati aktivnu datoteku\n");
      return;
   }
   fputs(aktivnaDatoteka,stdout);
}

void prikazi_sadrzaj_datoteke(FILE *fp){
   Bucket b=malloc(bucket_size());
   int i;
   for(i=0;i<BROJ_BAKETA;++i){
      fread_rasuta(b,i,fp);
      printf("ADRESA BAKETA: %d\n",i);
      print_bucket(b);
   }
   free(b);
   b=NULL;
}

Bucket trazi_slog_rasuta( int kljuc, int *adresa ,FILE*fp , int *pozUBaketu){
   printf("Trazim slog sa kljucem %d \n",kljuc);
   int adr;
   Bucket b=malloc(bucket_size());
   TerminNastave nadjenSlog;
   int adrMaticniBaket=get_address(kljuc);
   fread_rasuta(b,adrMaticniBaket,fp);
   printf("trazim u maticnom %d\n",adrMaticniBaket);
   *adresa=adrMaticniBaket;
   if (NULL==trazi_slog(b,kljuc,pozUBaketu)){
      adr=sledeca_adresa_baketa(adrMaticniBaket);
      while(adr!=adrMaticniBaket){
         printf("trazim u baketu %d\n",adr);

          fread_rasuta(b,adr,fp);
          nadjenSlog=trazi_slog(b,kljuc,pozUBaketu);
          if(nadjenSlog!=NULL){
               printf("Nasao u baketu %d\n",adr);
               *adresa=adr;
               break;
          }
          adr=sledeca_adresa_baketa(adr);
          if(adr==adrMaticniBaket){
               printf("Neuspesno trazenje!\n");
               *adresa=-1;
               free(b);
               b=NULL;
          }
      }
   }
   fflush(fp);
   return b;
}

void fseek_rasuta(FILE *fp, int adresa, long odakle){
   fseek(fp,adresa*bucket_size(),odakle);
}

void fread_rasuta(Bucket b,int adresa,FILE *fp){
   fseek_rasuta(fp,adresa,SEEK_SET);
   fread_check(b,bucket_size(),1,fp);

}

void fwrite_rasuta(Bucket b, int adresa, FILE *fp){
   fseek_rasuta(fp,adresa,SEEK_SET);
   fwrite_check(b,bucket_size(),1,fp);
   fflush(fp);
}
int h(int key){
   return key%BROJ_BAKETA;
}

int get_address(int key){
   return h(key);

}


void promeni_datum_vreme(int key,FILE *fp){
   int adr;
   int pozUBaketu;
   Bucket b=trazi_slog_rasuta(key,&adr,fp, &pozUBaketu);
   if(b!=NULL){
      printf("Nasli baket %d u kom je slog na poz %d, menjamo datum i vreme..\n",adr,pozUBaketu);
      promeni_datum_vreme_baket(b,pozUBaketu);
      fwrite_rasuta(b, adr,fp);
      free(b);
      b=NULL;

   }else{
      printf("Nema tog sloga u datoteci!\n");
   }
   fflush(fp);
}
#define MAX_IME_LEN 30










void upisi_slog(FILE *fp){
   int kljuc;
   int dummy,a;
   int matAdr;
   int adr;
   Bucket b;
   TerminNastave tn=malloc(termin_size());
   upisi_podatke(tn);
   kljuc=get_key(tn);
   matAdr=get_address(kljuc);
   b=trazi_slog_rasuta(kljuc,&a,fp,&dummy);
   if(b!=NULL){
      printf("Vec postoji taj slog u datoteci!\n");
      free(b);
      b=NULL;
   }else{
      b=malloc(bucket_size());
      fread_rasuta(b,matAdr,fp);
      adr=matAdr;
      while(get_emptySpaceCount(b)==0 && (adr=sledeca_adresa_baketa(adr))!=matAdr){
         printf("Trazimo mesto za prekoracioca...\n");
         fread_rasuta(b,adr,fp);//dok god nema mesta idemo na sledeci
      }

      if(get_emptySpaceCount(b)==0){
         printf("Puna datoteka!\n");
      }else{
         ubaci_u_baket(b,tn);
         fwrite_rasuta(b,adr,fp);
         printf("Uspesan upis! na adresu %d\n", adr);
      }
      
      free(b);
      b=NULL;
      

   }
   
   fflush(fp);
   free(tn);
   tn=NULL;

}


int sledeca_adresa_baketa(const int adresaBaketa){
	int sled_adresa=(adresaBaketa+1)%BROJ_BAKETA;
	assert(sled_adresa>=0 && sled_adresa<BROJ_BAKETA);
	return sled_adresa;

}

void izvadi_prekoracioca(const int pocetnaAdresa, FILE *fp){
   Bucket b=malloc(bucket_size());
   int adr;
   int baketBioPun;
   int pozUBaketu;
   adr=sledeca_adresa_baketa(pocetnaAdresa);
   assert(adr!=pocetnaAdresa);
   //samo ovo nam nece funkcionisati ako imamo datoteku sa 1 baket
   do{

      fread_rasuta(b,adr,fp);

      baketBioPun=get_emptySpaceCount(b)?0:1;
      pozUBaketu=trazi_odredjenog_prekoracioca( b, &get_address, pocetnaAdresa);
      if(pozUBaketu!=-1){
         printf("nasli smo u prekoracioca u baketu %d na poziciji %d\n",adr,pozUBaketu);
         TerminNastave tn=izbrisi_iz_baketa(b,pozUBaketu);
         fwrite_rasuta(b,adr,fp);
         fflush(fp);

         fread_rasuta(b,pocetnaAdresa,fp);

         ubaci_u_baket(b,tn);

         fwrite_rasuta(b,pocetnaAdresa,fp);
         fflush(fp);
         free(tn);
         tn=NULL;

         if(baketBioPun){
            izvadi_prekoracioca(adr,fp);

         }else{
            printf("Nema vise prekoracioca za shiftovanje!\n");
            break;
         }

      }else if(!baketBioPun){
         printf("Nema vise prekoracioca koje mozemo da prebacimo!\n");
         break;
      }


   }while( sledeca_adresa_baketa(adr)!=pocetnaAdresa );
   
   free(b);
   b=NULL;

}



void izbrisi_slog_rasuta(FILE *fp){
   int adresaBaketa,pozUBaketu,baketBioPun;
   int kljuc;
   printf("Upisite kljuc sloga koji hocete da izbrisete\n");
   kljuc=ucitaj_cifre();
   Bucket b=trazi_slog_rasuta(kljuc,&adresaBaketa, fp, &pozUBaketu);
   if(b!=NULL){
      baketBioPun=get_emptySpaceCount(b)?0:1;
      TerminNastave temp=izbrisi_iz_baketa(b,pozUBaketu);
      free(temp);
      temp=NULL;
      fwrite_rasuta(b,adresaBaketa,fp);
      if(baketBioPun){
         izvadi_prekoracioca(adresaBaketa,fp);

      }
      free(b);
      b=NULL;


   }else{

      printf("Nema sloga u datoteci\n");
   }

   fflush(fp);





}

char getChoice(void){
   char ch;
   printf("############################################\n");
   printf("##################* MENI *##################\n");
   printf("############################################\n\n");
   printf("Izaberite zeljenu opciju, tj ukucajte slovo:\n");
   printf("# f:Formiraj novu datoteku                 #\n");
   printf("# a:Izaberi datoteku kao aktivnu           #\n");
   printf("# p:Prikazi naziv aktivne datoteku         #\n");
   printf("# u:Upisi novi slog u aktivnu datoteku     #\n");
   printf("# s:Prikazi celokupan sadrzaj datoteke     #\n");
   printf("# d:Promeni datum i vreme sloga            #\n");
   printf("# x:Izbrisi slog iz aktivne datoteke       #\n");
   printf("# i:Izadji iz programa                     #\n");
   printf("############################################\n\n");

   ch=tolower(getchar());
   
   while(getchar()!='\n'){}
   while(!(ch=='f' || ch=='a' || ch=='i' || ch=='p' || ch=='x' || ch=='u' || ch=='s' || ch=='d' )){
      printf("Molim vas unesite jedno od slova f,a,p,u,s,d,x ili i\n");
      ch=getchar();
      while(getchar()!='\n'){}
   }
   return ch;
}
