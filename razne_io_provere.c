

#include <stdlib.h>
#include "razne_io_provere.h"


void get_limited_int(int *dest, int lowerLimit, int upperLimit){
    while( scanf("%d",dest)!=1 || *dest<lowerLimit || *dest>upperLimit){
      while(getchar()!='\n')
            ;;
      printf("Pogresan unos. Unesite broj od [%d-%d]\n", lowerLimit,upperLimit);
      
   }
   while(getchar()!='\n')
            ;;
}



void fgets_checked(char *buff, int num, FILE *fp){
   if(fgets(buff,num,fp)==NULL){
      if(ferror(fp)!=0){
         fprintf(stderr,"Desila se greska tokom citanja, ne garantujem za sadrzaj prosledjenog string 'buffera'\n");
         fprintf(stdout,"Ustvari, izlazim!\n");
         exit(EXIT_FAILURE);
      }else{
         fprintf(stdout,"Nema nista da se procita. Nastavljam...\n");
      }
   }
   char *newline;
   while((newline=strchr(buff,'\n'))==NULL){
      while(getchar()!='\n'){}
      printf("Uneli ste duzi fajl nego sto smo procitali. Maksimalno je %d\n", num-2);
      printf("Probajte ponovo!\n");
      fgets(buff,num,fp);
   }
   *newline='\0';
   
}

#define FORMAT_LENGTH 3
//prosledjuje joj se koliko maksimalno cifra moze da se ucita
//za sad samo radi za ogranicenja do 9 cifara
int ucitaj_cifre(){
   int broj;
   while(scanf("%6d",&broj)!=1 || getchar()!='\n' || broj<0){
         printf("Pogresan unos. Unesite pozitivan broj od maksimalno 6 cifara\n");
         while(getchar()!='\n')
                     ;;
   }
   
   return broj;
}

//zatvara i stavlja pointer na NULL
void safe_fclose(FILE *fp){
   if(fclose(fp)==EOF){
      
      printf("Neuspesno zatvaranje fajla. Izlazim!\n");
      exit(EXIT_FAILURE);
   }
   fp=NULL;
   
}

void fread_check(void *ptr, size_t size, size_t n, FILE *fp ){
   
   if(fread(ptr,size,n,fp)!=n){
      if(feof(fp)) assert(0);
      printf("An errurr ocurred while reading. Bye!\n");
      exit(EXIT_FAILURE);
   }   
   
}

void fwrite_check(void *ptr, size_t size, size_t n, FILE *fp ){
      if(fwrite(ptr,size,n,fp)!=n){
         if(feof(fp))assert(0);
         fprintf(stderr,"Desila se greska za vreme pisanja. Izlazim!\n");
         exit(EXIT_FAILURE);
      }
   
}

FILE* safe_fopen(char *fileName, char *mode){
   FILE *f;
   if((f=fopen(fileName,mode))==NULL){
      fprintf(stderr,"Neuspesno otvaranje fajla. Izlazim!\n");
      exit(EXIT_FAILURE);
   }
   return f;
   
}





