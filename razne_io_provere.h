#ifndef IO_PROVERE_H
#define IO_PROVERE_H
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void get_limited_int(int *dest, int lowerLimit, int upperLimit);



void fgets_checked(char *buff, int num, FILE *fp);
//zatvara i stavlja pointer na NULL
void safe_fclose(FILE *fp);
FILE* safe_fopen(char *fileName, char *mode);

void fread_check(void *ptr, size_t size, size_t n, FILE *fp );

void fwrite_check(void *ptr, size_t size, size_t n, FILE *fp );

//prosledjuje joj se koliko maksimalno cifra moze da se ucita
//za sad samo radi za ogranicenja 6 cifara
//PROMENI NASLOV
int ucitaj_cifre();

#endif
