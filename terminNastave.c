#include "terminNastave.h"
#include "razne_io_provere.h"
#include "datum_i_vreme.h"
#include <assert.h>
#include <stdlib.h>
#define BROJ_CIFARA_ID 6
#define NASLOV_MAX_LEN 50
#define OZN_PROS_MAX_LEN 7


 struct termin_nastave {
   int evidencioni_broj;
   datum datum_odrzavanja;
   vreme vreme_odrzavanja;
   char naslov[NASLOV_MAX_LEN];
   char oznaka_prostorije[OZN_PROS_MAX_LEN];
   int broj_studenata;

};

typedef struct termin_nastave termin_nastave;



int termin_size(){

   return sizeof(termin_nastave);

}


void set_evidencioni_broj(TerminNastave tn){
      printf("Unesite evidencioni broj od 6 cifara\n");
      tn->evidencioni_broj=ucitaj_cifre(6);

}

void set_broj_studenata(int *broj){
	printf("Unesite broj studenata koji je prisustvovao na predavanju!\n");
	get_limited_int(broj,0,100000);

}


void set_naslov(char* str){
   printf("Upisite naslov\n");
   fgets_checked(str,NASLOV_MAX_LEN+2,stdin);

}

void set_oznaka_prostorije(char* str){
   printf("Unesite oznaku prostorije!\n");
   fgets_checked(str,OZN_PROS_MAX_LEN+2,stdin);

}


void upisi_podatke(TerminNastave tn){
      set_evidencioni_broj(tn);
      set_datum(&(tn->datum_odrzavanja));
      set_vreme(&(tn->vreme_odrzavanja));
      set_naslov((tn->naslov));
      set_oznaka_prostorije((tn->oznaka_prostorije));
	   set_broj_studenata(&(tn->broj_studenata));
}

void set_datum_termin(TerminNastave tn){
      set_datum(&(tn->datum_odrzavanja));
}


void ispisi_podatke(TerminNastave tn){
   printf("Evidencioni broj: %d\n",tn->evidencioni_broj);

   printf("Datum: ");
   ispisi_datum(&(tn->datum_odrzavanja));
   fputs("Vreme: ",stdout);
   ispisi_vreme(&(tn->vreme_odrzavanja));
   printf("Naslov: %s\n",tn->naslov);
   printf("Oznaka prostorije: %s\n",tn->oznaka_prostorije);
   printf("Broj studenata koji je prisustvovao %d\n\n",tn->broj_studenata);


}

void init_termin(TerminNastave tn){
   tn->evidencioni_broj=-1;
   *(tn->naslov)='\0';
   make_empty_datum(&(tn->datum_odrzavanja));
   *(tn->oznaka_prostorije)='*';
   tn->oznaka_prostorije[1]='\0';
   make_empty_vreme(&(tn->vreme_odrzavanja));
   tn->broj_studenata=-1;
}

void set_vreme_termin(TerminNastave tn){
   set_vreme(&(tn->vreme_odrzavanja));
}

int get_key(TerminNastave tn){
   return tn->evidencioni_broj;

}

void termin_copy(TerminNastave src,TerminNastave dest){
   dest->evidencioni_broj=src->evidencioni_broj;
   dest->datum_odrzavanja=src->datum_odrzavanja;
   dest->vreme_odrzavanja=src->vreme_odrzavanja;
   dest->broj_studenata=src->broj_studenata;
   strcpy(dest->oznaka_prostorije,src->oznaka_prostorije);
   strcpy(dest->naslov,src->naslov);
}

int su_jednaki(TerminNastave tn1, TerminNastave tn2){

    return get_key(tn1)==get_key(tn2);

}





