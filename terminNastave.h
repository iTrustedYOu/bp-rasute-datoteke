#ifndef TERMIN_NASTAVE_H
#define TERMIN_NASTAVE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRUE 1
#define FALSE 0

typedef struct termin_nastave *TerminNastave;


void fgets_checked(char *buff, int num, FILE *fp);
int get_key(TerminNastave tn);
void set_vreme_termin(TerminNastave tn);
void set_datum_termin(TerminNastave tn);

 void set_evidencioni_broj(TerminNastave tn);
 TerminNastave novi_terminNastave();
 void upisi_podatke(TerminNastave tn);
 void ispisi_podatke(TerminNastave tn);
 //vraca velicinu objekta na koji pokazivac TerminNastave
 //pokazuje
 int termin_size();

 void init_termin(TerminNastave tn);
 void termin_copy(TerminNastave src, TerminNastave dest);


#endif
